﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Keeps a track of no. of Green tiles.
/// And broadcasts Level Finished when all the tiles are Green.
/// </summary>
public class ProgressTracker : MonoBehaviour
{
    [Header("Events to Subscribe")]
    [SerializeField]
    Vector2EventChannelSO gridDimensionEvent;

    [SerializeField]
    VoidEventChannelSO tileTurnedGreenEvent;
    
    [Header("Events to Raise")]
    [SerializeField]
    VoidEventChannelSO levelFinishedEvent;

    int totalTiles, greenTiles;

    private void OnEnable()
    {
        gridDimensionEvent.OnVector2ValueRaised += InitialiseCount;
        tileTurnedGreenEvent.OnVoidEventRaised += IncreaseCount;
    }

    private void OnDisable()
    {
        gridDimensionEvent.OnVector2ValueRaised -= InitialiseCount;
        tileTurnedGreenEvent.OnVoidEventRaised -= IncreaseCount;
    }

    private void Start()
    {
        greenTiles = 0;
    }

    private void InitialiseCount(Vector2 val)
    {
        totalTiles = (int)(val.x * val.y);
    }

    void IncreaseCount()
    {
        greenTiles += 1;
        if (greenTiles == totalTiles)
        {
            levelFinishedEvent.RaiseEvent();
        }
    }
}
