﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used to raise & listen to Events that has one float argument.
/// </summary>
[CreateAssetMenu(fileName = "New Float Event Channel", menuName = "Events/Float Event Channel")]
public class FloatEventChannelSO : ScriptableObject
{
    public UnityAction<float> OnFloatValueRaised;

    public void RaiseEvent(float val)
    {
        OnFloatValueRaised?.Invoke(val);
    }
}