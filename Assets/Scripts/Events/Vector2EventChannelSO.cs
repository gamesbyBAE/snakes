﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used to raise & listen to Events that has one Vector2 argument.
/// </summary>
[CreateAssetMenu(fileName = "New Vector2 Event Channel", menuName = "Events/Vector2 Event Channel")]
public class Vector2EventChannelSO : ScriptableObject
{
    public UnityAction<Vector2> OnVector2ValueRaised;

    public void RaiseEvent(Vector2 val)
    {
        OnVector2ValueRaised?.Invoke(val);
    }
}