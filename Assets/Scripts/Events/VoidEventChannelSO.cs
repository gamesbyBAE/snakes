﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used to raise & listen to Events that has no argument.
/// </summary>
[CreateAssetMenu(fileName = "New Void Event Channel", menuName = "Events/Void Event Channel")]
public class VoidEventChannelSO : ScriptableObject
{
    public UnityAction OnVoidEventRaised;

    public void RaiseEvent()
    {
        OnVoidEventRaised?.Invoke();
    }
}