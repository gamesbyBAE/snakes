﻿using UnityEngine;

/// <summary>
/// Rescales the arena/ground according to the grid dimensions.
/// </summary>
public class ArenaScaler : MonoBehaviour
{
    [Header("Events to Subscribe")]
    [SerializeField]
    Vector2EventChannelSO gridDimensionsEvent;

    [Header("Events to Raise")]
    [SerializeField]
    VoidEventChannelSO rescaledEvent;

    private void OnEnable()
    {
        gridDimensionsEvent.OnVector2ValueRaised += Rescale;
    }
    private void OnDisable()
    {
        gridDimensionsEvent.OnVector2ValueRaised -= Rescale;
    }

    private void Rescale(Vector2 arg0)
    {
        transform.localScale = new Vector3(transform.localScale.y * arg0.x, transform.localScale.y, transform.localScale.y * arg0.y);

        rescaledEvent.RaiseEvent();
    }
}
