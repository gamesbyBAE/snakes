﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the snake, i.e., moves it forward and rotates according
/// to SteeringWheel input and manages animations.
/// </summary>
public class SnakeController : MonoBehaviour
{
    [SerializeField]
    float forwardSpeed;

    [SerializeField]
    float turningSpeed;

    [SerializeField]
    string animatorParameter;

    [SerializeField]
    StringVariableSO wallTag;

    [Header("Events to Subscribe")]
    [SerializeField]
    FloatEventChannelSO steerValueEvent;

    Rigidbody rb;
    float steerValue;

    Vector3 targetPos;
    Vector3 targetRot;

    Animator snakeAnimationController;

    float collisionAngle;
    private void OnEnable()
    {
        steerValueEvent.OnFloatValueRaised += SetSteerValue;
    }
    private void OnDisable()
    {
        steerValueEvent.OnFloatValueRaised -= SetSteerValue;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        snakeAnimationController = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        MoveForward();
        Turn();
    }

    void MoveForward()
    {
        targetPos = transform.position;
        targetPos.y = 0;
        targetPos += transform.forward * forwardSpeed * Time.fixedDeltaTime;
        rb.MovePosition(targetPos);
    }

    void Turn()
    {
        targetRot = transform.rotation.eulerAngles;
        targetRot.y += steerValue * turningSpeed * Time.fixedDeltaTime; // To rotate only around y-axis.
        rb.MoveRotation(Quaternion.Euler(targetRot)); // Takes in a Quaternion, hence, converting Euler Angle to Quaternion.
    }

    private void SetSteerValue(float arg0)
    {
        steerValue = arg0;

        snakeAnimationController.SetFloat(animatorParameter, steerValue); // Updating blend tree values for animations.
    }

    // Reflecting after Collision with Walls.
    /*private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag(wallTag.StringValue))
        {
            Vector3 normal = collision.contacts[0].normal;
            Vector3 collDir = (rb.transform.position - collision.contacts[0].point).normalized;
            collisionAngle = (Vector3.Angle(collDir, normal));

            rb.transform.rotation = Quaternion.Euler(rb.transform.rotation.y, rb.transform.rotation.y + collisionAngle, rb.transform.rotation.z);
        }
    }*/
}
