﻿using UnityEngine;
using UnityEngine.EventSystems;
public class SteeringWheel : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField]
    float maxSteerAngle;
    
    [SerializeField]
    float releaseSpeed;

    [Header("Event to Raise")]
    [SerializeField]
    FloatEventChannelSO steerValueEvent;

    RectTransform wheelRectTransform;
    bool wheelHeld = false;

    float wheelAngle = 0f;
    float lastWheelAngle = 0f;

    Vector2 center;
    
    float outPut;

    private void Start()
    {
        wheelRectTransform = GetComponent<RectTransform>();   
    }

    void Update()
    {
        if (!wheelHeld && wheelAngle != 0f)
        {
            float DeltaAngle = releaseSpeed * Time.deltaTime;
            if (Mathf.Abs(DeltaAngle) > Mathf.Abs(wheelAngle))
                wheelAngle = 0f;
            else if (wheelAngle > 0f)
                wheelAngle -= DeltaAngle;
            else
                wheelAngle += DeltaAngle;
        }
        wheelRectTransform.localEulerAngles = new Vector3(0, 0, -maxSteerAngle * outPut);
        outPut = wheelAngle / maxSteerAngle;

        steerValueEvent.RaiseEvent(outPut);
    }
    public void OnPointerDown(PointerEventData data)
    {
        wheelHeld = true;
        center = RectTransformUtility.WorldToScreenPoint(data.pressEventCamera, wheelRectTransform.position);
        lastWheelAngle = Vector2.Angle(Vector2.up, data.position - center);
    }
    public void OnDrag(PointerEventData data)
    {
        float NewAngle = Vector2.Angle(Vector2.up, data.position - center);
        if ((data.position - center).sqrMagnitude >= 400)
        {
            if (data.position.x > center.x)
                wheelAngle += NewAngle - lastWheelAngle;
            else
                wheelAngle -= NewAngle - lastWheelAngle;
        }
        wheelAngle = Mathf.Clamp(wheelAngle, -maxSteerAngle, maxSteerAngle);
        lastWheelAngle = NewAngle;
    }
    public void OnPointerUp(PointerEventData data)
    {
        OnDrag(data);
        wheelHeld = false;
    }
}