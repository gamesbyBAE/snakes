﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Loads various scenes according to raised events. Avoiding usage of Singleton.
/// This works in Editor only, where it works according to the raised events.
/// 
/// To make it work in Builds, add the SO Asset file to
/// Project Settings -> Player -> Other Settings -> Optimization -> Preloaded Assets
/// Unity will load such assets in runtime before scene loading and will keep them alive
/// until the game/app has been terminated.
/// 
/// But this way works only in Builds. But since we are using events as well, it works
/// in Editor as well without need of additional scripts to run it in Editor for testing purposes.
/// </summary>
[CreateAssetMenu(fileName = "Scene Loader SO", menuName = "Scene Loader")]
public class SceneLoader : ScriptableObject
{
    [Header("Scene Names")]
    [SerializeField] string gameSceneName = default;

    [Header("Events to Subscribe")]
    [SerializeField] VoidEventChannelSO startGameEvent = default;
    [SerializeField] VoidEventChannelSO restartLevelEvent = default;
    [SerializeField] VoidEventChannelSO quitEvent = default;

    private void OnEnable()
    {
        startGameEvent.OnVoidEventRaised += PlayGame;
        restartLevelEvent.OnVoidEventRaised += RestartLevel;
        quitEvent.OnVoidEventRaised += QuitGame;
    }
    private void OnDisable()
    {
        startGameEvent.OnVoidEventRaised -= PlayGame;
        restartLevelEvent.OnVoidEventRaised -= RestartLevel;
        quitEvent.OnVoidEventRaised -= QuitGame;
    }
    void PlayGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadSceneAsync(gameSceneName);
    }


    void RestartLevel()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void QuitGame()
    {
        Application.Quit();
    }
}