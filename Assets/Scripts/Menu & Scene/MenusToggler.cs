﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenusToggler : MonoBehaviour
{
    [SerializeField]
    GameObject pauseMenuGO;
    
    [SerializeField]
    GameObject leveFinishedMenuGO;

    [Header("Events to Subscribe")]
    [SerializeField]
    VoidEventChannelSO pauseButtonPressedEvent;

    [SerializeField]
    VoidEventChannelSO levelFinishedEvent;

    Canvas menuCanvas;

    bool showPauseMenu;

    private void OnEnable()
    {
        pauseButtonPressedEvent.OnVoidEventRaised += ShowPauseMenu;
        levelFinishedEvent.OnVoidEventRaised += ShowLevelFinishedMenu;
    }
    private void OnDisable()
    {
        pauseButtonPressedEvent.OnVoidEventRaised -= ShowPauseMenu;
        levelFinishedEvent.OnVoidEventRaised -= ShowLevelFinishedMenu;
    }

    void Start()
    {
        menuCanvas = GetComponent<Canvas>();
        menuCanvas.enabled = false;

        showPauseMenu = false;

        pauseMenuGO.SetActive(false);
        leveFinishedMenuGO.SetActive(false);
    }

    // Using it for both showing & hiding Pause Menu
    // when user selects Resume.
    private void ShowPauseMenu()
    {
        showPauseMenu = !showPauseMenu;

        if (showPauseMenu)
        {
            Time.timeScale = 0;

            menuCanvas.enabled = true;

            pauseMenuGO.SetActive(true);
            leveFinishedMenuGO.SetActive(false);
        }
        else
        {
            Time.timeScale = 1;

            pauseMenuGO.SetActive(false);
            
            menuCanvas.enabled = false;
        }
    }

    private void ShowLevelFinishedMenu()
    {
        Time.timeScale = 0;

        menuCanvas.enabled = true;

        pauseMenuGO.SetActive(false);
        leveFinishedMenuGO.SetActive(true);
    }

}
