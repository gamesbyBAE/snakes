﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PickupCounter : MonoBehaviour
{
    [Header("Events to Raise")]
    [SerializeField]
    IntEventChannelSO countValueEvent;

    [Header("Events to Subscribe")]
    [SerializeField]
    VoidEventChannelSO collectedPickupEvent;

    [SerializeField]
    VoidEventChannelSO countRequestedEvent;

    int count;
    TextMeshProUGUI countText;

    private void OnEnable()
    {
        collectedPickupEvent.OnVoidEventRaised += IncrementValue;
        countRequestedEvent.OnVoidEventRaised += SendCountValue;
    }
    private void OnDisable()
    {
        collectedPickupEvent.OnVoidEventRaised -= IncrementValue;
        countRequestedEvent.OnVoidEventRaised -= SendCountValue;
    }

    private void Start()
    {
        count = 0;

        countText = GetComponent<TextMeshProUGUI>();
        countText.SetText(count.ToString());
    }

    private void IncrementValue()
    {
        count += 1;
        countText.SetText(count.ToString());
    }

    void SendCountValue()
    {
        countValueEvent.RaiseEvent(count);
    }
}
