﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Spawns crates in empty places at regular intervals.
/// When crates are destroyed, places the drops in that position.
/// </summary>
public class SpawnPickup : MonoBehaviour
{
    [SerializeField]
    Pooler cratesPool;

    // Spawning
    [Header("Spawning")]
    [Tooltip("Maximum no of drops that should be allowed on the arena at once.")]
    [SerializeField]
    int maxDropsActive = 5;

    [Tooltip("Time interval after which to spawn a new ability.")]
    [SerializeField]
    float timeInterval = 5;

    WaitForSeconds waitTime;
    WaitUntil waitUntilReadyToSpawn;

    // Spawn Region
    [Header("Spawn Region")]
    [SerializeField]
    Collider groundGameObject;

    [Tooltip("x-value to subtract from ground's width(total x-distance), to avoid spawning at edges of the ground.")]
    [SerializeField]
    float xOffset;

    [Tooltip("z-value to subtract from ground's width(total z-distance), to avoid spawning at edges of the ground.")]
    [SerializeField]
    float zOffset;

    float xPosMin, xPosMax;
    float zPosMin, zPosMax;

    [Tooltip("Radius to create a sphere which checks for empty space suitable for spawning.")]
    [SerializeField]
    float spawnPositionCheckerRadius;

    [Tooltip("Layer which the Spawn Position Checker checks for collision with.")]
    [SerializeField]
    LayerMask collisionLayerMask;

    [Tooltip("No. of times loop checks for new position, in case it overlaps with other objects to avoid being stuck in an infinite loop.")]
    [SerializeField]
    int maxPositionCheckerQuery;

    // Tags
    [SerializeField]
    StringVariableSO pickUpTag;
    
    [SerializeField]
    StringVariableSO tileTag;

    // Events
    [Header("Events to Subscribe")]
    [SerializeField]
    VoidEventChannelSO rescaledArenaEvent;

    [SerializeField]
    GameObjectEventChannelSO usedDropGO;

    Vector2 newDropPos;
    int activeDropsCount;
    IEnumerator spawnCrateCoroutine;

    Vector3 newPosition;

    private void OnEnable()
    {
        //cratePos.OnVector2ValueRaised += StoreCratePos;
        //newDropGO.OnGameObjectValueRaised += SpawnDrop;
        usedDropGO.OnGameObjectValueRaised += DecreaseActiveDropCount;
        rescaledArenaEvent.OnVoidEventRaised += BeginSpawning;
    }
    private void OnDisable()
    {
        //cratePos.OnVector2ValueRaised -= StoreCratePos;
        //newDropGO.OnGameObjectValueRaised -= SpawnDrop;
        usedDropGO.OnGameObjectValueRaised -= DecreaseActiveDropCount;
        rescaledArenaEvent.OnVoidEventRaised -= BeginSpawning;

        StopCoroutine(spawnCrateCoroutine);
    }
    private void Start()
    {
        activeDropsCount = 0;
        newDropPos = Vector2.zero;

        waitTime = new WaitForSeconds(timeInterval);
    }

    void BeginSpawning()
    {
        xPosMin = groundGameObject.bounds.min.x + xOffset;
        xPosMax = (groundGameObject.bounds.max.x * groundGameObject.gameObject.transform.localScale.x) - xOffset;
        zPosMin = groundGameObject.bounds.min.z + zOffset;
        zPosMax = (groundGameObject.bounds.max.z * groundGameObject.gameObject.transform.localScale.z) - zOffset;

        spawnCrateCoroutine = SpawnCrateCountdown();
        StartCoroutine(spawnCrateCoroutine);
    }

    IEnumerator SpawnCrateCountdown()
    {
        //yield return new WaitForSeconds(2); //Starts spawning after few seconds.

        while (true)
        {
            yield return waitTime;

            if (activeDropsCount < maxDropsActive)
            {
                Vector3 newSpawnPos = GetSpawnPosition();

                if (newSpawnPos != Vector3.zero)
                {
                    GameObject receivedGO = cratesPool.GetFromPool();
                    receivedGO.SetActive(true);
                    receivedGO.transform.position = new Vector3(newPosition.x,receivedGO.transform.position.y,newPosition.z);
                    activeDropsCount += 1;
                }
            }
        }
    }

    private Vector3 GetSpawnPosition()
    {
        int attemptCount = 0;
        while (attemptCount < maxPositionCheckerQuery)
        {
            float xPos = Random.Range(xPosMin, xPosMax);
            float zPos = Random.Range(zPosMin, zPosMax);
            newPosition = new Vector3(xPos, 0, zPos); //Can make it local if OnDrawGizmos is not being used.

            // Checks if any other GOs on layer, 'Default' collide/overlap with the box of dimensions 'positionCheckerDimensions'
            // at the new randomly generated position, 'newPosition'.
            Collider[] collidersList = new Collider[10];
            int overlapCount = Physics.OverlapSphereNonAlloc(newPosition, spawnPositionCheckerRadius, collidersList, collisionLayerMask); // Generates no garbage unlike OverlapBox.
            for (int i = 0; i < overlapCount; i++)
            {
                if (collidersList[i].gameObject.CompareTag(tileTag.StringValue) && collidersList[i].gameObject.GetComponent<TileBehaviour>().IsTileGreen == false)
                {
                    return newPosition;
                }
            }

            attemptCount += 1;
        }
        return Vector3.zero;
    }

   /* //Stores Crate's position when it has been destroyed by the player.
    //Raises an event to get a new Drop game object.
    void StoreCratePos(Vector2 val)
    {
        newDropPos = val;
        getDropGO.RaiseEvent();
    }

    //Updates the position of the received Drop game object.
    void SpawnDrop(GameObject go)
    {
        go.transform.position = new Vector3(newDropPos.x, 0, newDropPos.y);
    }*/

    // Decrements the counter when the Drop has been picked up by the Player.
    void DecreaseActiveDropCount(GameObject go)
    {
        activeDropsCount -= 1;
        if (activeDropsCount < 0)
        {
            activeDropsCount = 0;
        }
    }

    //To visualise the box created to check if the space is empty in 
    //GetSpawnPosition method.
    //To make it work declare 'newPosition' globally rather than locally
    //in Line-116.
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Gizmos.DrawWireCube(newPosition, positionCheckerDimensions);
        Gizmos.DrawWireSphere(newPosition, spawnPositionCheckerRadius);
    }
}
