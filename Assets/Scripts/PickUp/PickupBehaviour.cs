﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// When collides with Player, raises event broadcasting that the player has collected
/// the pickup and sends itself back into the pool.
/// Also sends itself back into the pool if it has not been collected within the specified time.
/// </summary>
public class PickupBehaviour : MonoBehaviour
{
    [SerializeField][Tooltip("Time in seconds before it is set inactive.")]
    float lifeTime;

    [SerializeField]
    StringVariableSO playerTag;

    [Header("Events to Raise")]
    [SerializeField]
    GameObjectEventChannelSO usedGOEvent;

    [SerializeField]
    VoidEventChannelSO collectedPickupEvent;

    IEnumerator coroutine;
    WaitForSeconds waitTime;

    private void OnEnable()
    {
        coroutine = CountDown();
        StartCoroutine(coroutine);
    }

    private void OnDisable()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
    }
    private void Start()
    {
        waitTime = new WaitForSeconds(lifeTime); // Caching rather than creating a new instance everytime.
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(playerTag.StringValue))
        {
            usedGOEvent.RaiseEvent(this.gameObject); //Raising the event to put this gameobject back into the pool.
            collectedPickupEvent.RaiseEvent();
            StopCoroutine(coroutine);
        }
    }

    // Coroutine that waits for specified time
    // before sending itself back into the pool.
    IEnumerator CountDown()
    {
        yield return waitTime;
        usedGOEvent.RaiseEvent(this.gameObject);
    }
}
