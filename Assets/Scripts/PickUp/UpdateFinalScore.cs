﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UpdateFinalScore : MonoBehaviour
{
    [Header("Events to Raise")]
    [SerializeField]
    VoidEventChannelSO countRequestEvent;
    
    [Header("Events to Subscribe")]
    [SerializeField]
    IntEventChannelSO countValueEvent;

    TextMeshProUGUI countText;

    private void OnEnable()
    {
        countValueEvent.OnIntValueRaised += UpdateText;

        countRequestEvent.RaiseEvent();
    }

    private void OnDisable()
    {
        countValueEvent.OnIntValueRaised -= UpdateText;
    }

    private void Start()
    {
        countText = GetComponent<TextMeshProUGUI>();
    }

    private void UpdateText(int arg0)
    {
        countText.text = arg0.ToString();
    }
}
