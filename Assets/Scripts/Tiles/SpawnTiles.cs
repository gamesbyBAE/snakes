﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Generates a grid of tiles according to the entered dimensions.
/// </summary>
public class SpawnTiles : MonoBehaviour
{
    [SerializeField]
    Vector2 gridDimensions;

    [Space(10)][SerializeField]
    GameObject tilePrefab;

    [Header("Events to Raise")]
    [SerializeField]
    Vector2EventChannelSO gridDimensionsEvent;

    private void Start()
    {
        gridDimensionsEvent.RaiseEvent(gridDimensions);

        // Generating tiles in a grid of dimensions (gridDimensions.x * gridDimensions.y)
        for (int i = 0; i < gridDimensions.x; i++)
        {
            for (int j = 0; j < gridDimensions.y; j++)
            {
                GameObject tile = Instantiate(tilePrefab, transform);
                tile.transform.position = new Vector3(i, 0, j);
            }
        }
    }
}
