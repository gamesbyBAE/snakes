﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Changes the color of the tile to GREEN when Player/Snake goes over it.
/// </summary>
public class TileBehaviour : MonoBehaviour
{
    [SerializeField] [Tooltip("Color which will be applied when the Snake goes over it.")]
    Color traversedColor;

    [SerializeField]
    StringVariableSO playerTag;

    [Header("Events to Raise")]
    [SerializeField]
    VoidEventChannelSO turnedGreenEvent;

    Material tileMaterial;
    
    bool isGreen;
    public bool IsTileGreen => isGreen;

    private void Awake()
    {
        tileMaterial = GetComponent<Renderer>().material;
        isGreen = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(playerTag.StringValue) && !isGreen)
        {
            tileMaterial.color = traversedColor;
            isGreen = true;

            turnedGreenEvent.RaiseEvent();
        }
    }
}
